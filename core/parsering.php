<?php
namespace core{

	use \core\parser as parser;	
	
	class parsering  {

		private $db;
		private $curl;

		public static function app() {
			return new self;
		}

		private function __construct(){
			$this->db = \core\sql::app();
			
			 $headers = array(
                'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
                'Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3'
            );
            
            $this->curl = curl::app(DOMAIN)
                             ->headers(1)
                             ->referer('http://www.google.com')
                             ->agent('Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0')
                             ->add_headers($headers);
		}

		
		public function load_list($fname){
			$news = $this->curl->request(REQUEST);
			
			$p = parser::app($news['html']);

			$i = 0;
			$news = [];

			while ($i < COUNT_NEWS){
				if($p->moveTo('<item') === -1){
					break;
				}
				$tmp = [];

				$p->moveTo('<item');
				$p->moveAfter('<title>');
				$tmp['title'] = $p->readTo('<');

				$p->moveAfter('<link>');
				$tmp['url_link'] = $p->readTo('<');
				$tmp['link'] = substr($tmp['url_link'], strlen(DOMAIN));

				$p->moveAfter('<![CDATA[');
				$description = $p->readTo(']]>');

				$p->moveAfter('<pubDate>');
				$p->moveAfter(', '); 
				$tmp['date'] = $p->readTo(' +0300</');

				$p->moveAfter('<enclosure url="');
				$img = $p->readTo('"');

				if(in_array(-1, $tmp) || in_array('', $tmp)){
					continue;
				}

				$news[$i]['title'] = $tmp['title'];
				$news[$i]['link'] = $tmp['link'];
				$news[$i]['content'] = $description;
				
				$news[$i]['name'] = str_replace('/', '', $tmp['link']);

				$dt = explode('/', $tmp['link']);
				$news[$i]['dt'] = $dt[2].'-'. $dt[3].'-'. $dt[4].' '.substr($tmp['date'], -8);

				if($img !== -1 || $img != ''){
					$news[$i]['img'] = $img;
				}

				$i++;
			} 
			
			return file_put_contents($fname, json_encode($news));
		}

		
		
		public function load_list111($fname){
			$data = $this->curl->request(REQUEST);
			
			if(empty($data['html'])){
				return false;
			}
		
			return file_put_contents($fname , json_encode($data['html']));
		}

		
		public function parse_list($fname){
			
			if (!file_exists($fname)) {
				return false;
			} 

			$news = json_decode(file_get_contents($fname), true);
			
			foreach($news as $n){
				$q = "SELECT * FROM news WHERE dt = '".$n['dt'].
						"' AND link = '".$n['link']."'";

				$res = $this->db->select($q);

				if(!count($res)){
					$this->db->insert('news', $n);
				}		
			}
		}

		public function load_every(){	
			$news = $this->db->select("SELECT * FROM news");

			if(empty($news)){
				return false;
			}

			set_time_limit(0);

			foreach($news as $new){
				$data = $this->curl->request($new['link']);

				file_put_contents(PATH_NEWS_EVERY . $new['name'], $data['html']);
				sleep(mt_rand(0, 1));
			}
		}


		public function parse_every(){
			$news = $this->db->select("SELECT * FROM news");

			if(empty($news)){
				return false;
			}

			foreach($news as $new){
				$fname = PATH_NEWS_EVERY . $new['name'];
				
				$one = file_get_contents($fname);
				$p = parser::app($one);

				$p->moveTo('itemprop="articleBody">');

				$str = [];
				$read = true;

				while ($read){	
					$p->moveAfter('<p>');
					$tmp = $p->readTo('</p>');

					if($tmp !== -1 && $tmp !== ''){
						$str[] = $tmp;
					}else{
						$read = false;
					}
				}
				$str = implode("\n", $str);

				if(strlen($str) > 0){	
					$this->db->update('news', ['content'=>$str], 'id = '. $new['id']);
				}
				
				if(file_exists($fname)){
					unlink($fname);
				}
			}
		}
	}
}
