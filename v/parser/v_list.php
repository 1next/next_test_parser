<div class="col-md-12">
 <div class="progress">
      <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="3" aria-valuemin="0" style="width: 0%">
      </div>
    </div>
	
	<p>Новостей: <span class="count"><?=count($data)?></span></p>
   <ul class="news">
		<? foreach ($data as $d): ?>
			<li> <?=$d['dt']?> <a href="/news/view/<?=$d['id']?>"><?=$d['title']?></a></li>
		<? endforeach ;?>
	</ul>
	
</div>