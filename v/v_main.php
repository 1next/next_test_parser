<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title><?=$title?></title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <style> li {list-style-type: none;}</style>
</head>
<body>
  <div class="container">
    <div class="row">
       <div class="start-btn col-md-12 text-center">
		<a class="btn btn-primary" href="/">Список</a>
		<button type="button" id="start" class="btn btn-success">Получить последние новости</button>
		<button type="button" id="getcsv" class="btn btn-danger">Экспорт за сутки в csv-файл</button>
	  	<p><span id="progress"></span></p> 
		<center><h2><?=$title?></h2></center>
	</div>	
      <?=$content?>
    </div>
  </div>
  <script>
    _PHP = <?=json_encode($js_vars)?>;
  </script>
  <? foreach($scripts as $script): ?>
    <script src="/media/js/<?=$script?>.js"></script>
  <? endforeach; ?>
</body>
</html>