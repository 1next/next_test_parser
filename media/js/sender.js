$(function(){
    var c = _PHP.c;
    var count = _PHP.count;
    
    var l = count*3+1;
    var done = 0;
    var $progress = $('#progress');
    var $progressBar = $('.progress-bar');
    var $logo = $('.logo img');
    var $log = $('#log');
    
    $('#start').click(function(){
        $(this).hide();
        loadlist();
    });
 

    function loadlist(){ 
        $progress.html('Загрузка списка новостей ...');
		
         $.ajax({
            url: '/' + c + '/one', 
            type: 'post',
            data: '/rss/news',
            cache: false,
            success: function(msg){
                done++;
                $progress.html('Список загружен');
                progressBar(false, msg);
                parselist();
            },
            error: function(){
                progressBar(true);
		alert('Ошибка загрузки списка новостей.');
                document.location.href = '/';
            }
         }); 
     }
   function parselist(){ 
        $progress.html('Парсинг списка ...');
		
         $.ajax({
            url: '/' + c + '/parselist',
            type: 'post',
            data: '/rss/news',
            cache: false,
            success: function(msg){
                done = done + count;
                $progress.html('Список готов.');
                progressBar(false, msg);
                loadevery();
            },
            error: function(){
                progressBar(true);
		alert('Ошибка парсинга списка.');
                document.location.href = '/';
            }
         }); 
     }
     
    function loadevery(){ 
        $progress.html('Загрузка содержимого каждой новости ...');
		
         $.ajax({
            url: '/' + c + '/loadevery', 
            type: 'post',
            data: '/rss/news',
            cache: false,
            success: function(msg){
                done = done + count;
                $progress.html('Загружено.');
                progressBar(false, msg);
	        parseevery();
            },
            error: function(){
                progressBar(true);
		alert('Ошибка загрузки содержимого новостей.');
                document.location.href = '/';
            }
         }); 
    }
     
    function parseevery(){ 
        $progress.html('Парсинг каждой новости ...');
		
         $.ajax({
            url: '/' + c + '/parseevery', 
            type: 'post',
            data: '/rss/news',
            cache: false,
            success: function(msg){
                done = done + count;
                $progress.html('Список новостей обновлен.');
                progressBar(false, msg);
		$('#start').show();
                document.location.href = '/';
            },
            error: function(){
                progressBar(true);
		alert('Ошибка парсинга новостей.');
                document.location.href = '/';
            }
         }); 
     }

     function progressBar(err, msg) {
        var percent = ((100 / l) * done).toFixed(2) + '%';

        if(err) {
            $progressBar.removeClass('progress-bar-success active').addClass('progress-bar-danger');
            return;
        }

        if(done == l) {
            $progressBar.removeClass('active progress-bar-striped');
        }

        $progressBar.css('width', percent);
        $logo.css('left', percent);
        $progressBar.html(percent);
        $log.append('<div>' + done + ': '+ msg + '</div>').scrollTop($log.prop('scrollHeight'));
     }
     
     
    $('#getcsv').click(function(){
       $progress.html('генерирую CSV-файл');
       
        $.ajax({
                url: '/' + c + '/getcsv', 
                type: 'post',
                data: 'getcsv',
                cache: false,
                success: function(){
                    $progress.html('Файл готов.');
                    document.location.href = '/news.csv';
                },
                error: function(){
                    alert('Ошибка генерации CSV-файла.');
                    document.location.href = '/';
                }
        });
    });

});