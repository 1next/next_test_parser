<?php

	define('DOMAIN', 'https://lenta.ru');
	define('REQUEST', '/rss/news');
	define('LOAD_CONFIG', 'news.cnf');
	define('COUNT_NEWS', 5);
	define('PATH_ALL_NEWS', 'data/listnews');
	define('PATH_NEWS_EVERY', 'data/news_every/');
	define('FILE_CSV', 'news.csv');
	
    define('MYSQL_SERVER', 'localhost');
    define('MYSQL_USER', 'root');
    define('MYSQL_PASSWORD', '');
    define('MYSQL_DB', 'parser');
