<?php

namespace controllers{
    use \core\curl as curl;
	use \core\parser as parser;
	use \core\parsering as parsering;
	
    class news extends sender{
        public function action_data(){
            $this->title = 'Новости';
            $this->data = $this->db->select("SELECT * FROM news ORDER BY dt DESC");
            $this->js_vars['c'] = 'news';
			$this->js_vars['count'] = COUNT_NEWS;
		}
        
        public function action_view(){
			
			if($this->params[1] == 'view'){
				$id = (int)$this->params[2];

				$res = $this->db->select("SELECT * FROM news WHERE id = $id");
				$this->data = $res[0];
			}
		}
        public function action_one(){
			$p = parsering::app();
			$p->load_list(PATH_ALL_NEWS);
			
		}
		
		public function action_parselist(){
			$p = parsering::app();
			$p->parse_list(PATH_ALL_NEWS);
			
		}

		public function action_loadevery(){
			$p = parsering::app();
			$p->load_every();
		}
		 
		public function action_parseevery(){
			$p = parsering::app();
			$p->parse_every();
		}
		
		public function action_getcsv(){
	
			$news = $this->db->select("SELECT * FROM news WHERE dt >= CURDATE()");

			if(empty($news)){
				return false;
			}
			
			if(file_exists(FILE_CSV)){
				unlink(FILE_CSV);
			}

			$fp = fopen(FILE_CSV, 'w');

			foreach ($news as $new) {
				fputcsv($fp, [$new['title'],$new['dt'],DOMAIN.$new['link']]);
			}

			fclose($fp);
		}
    }
}